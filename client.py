import socket            # a library used to create the client socket

print("Enter required item\n") # a prompt for the user to input the item he/she is looking for
if input()=="gloves":          # in this example gloves are the only option but the idea is for there to be multiple hardcoded options
 Port_Number = 6000           # port number of the destination port of the server socket
 Server_IP= "192.168.1.22"     # ip address of the destination server socket
 Header_Capacity =64           # [Intialising] an intial header space to allow the first message from th client to arrive 
 Server_Address = (Server_IP, Port_Number) # The server address consists of the destination port and the servers local IP address
 Disconnect = "end"            #[Intialising] The message from the client that will be used for closing the connection
 client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # This is were the client socket is created on the local network (AF_INET) using TCP (SOCK_STREAM)
 client.connect(Server_Address)                             # The previously established client socket connects to the server socket

def send(message):                             #A function used for sending ASCII messages to the server
    message = message.encode('utf-8')          # Encodes the string into a bytes like object
    message_length = len(message)              # lenght of message being sent
    send_length = str(message_length).encode('utf-8')# represents the message that we want to send
    send_length += b' ' * (Header_Capacity - len(send_length)) # padding the message by subtracting the lenght of the message from the header
    client.send(send_length)                   # send the lenght of the message
    client.send(message)                       # send the content of the message
    print(client.recv(2048).decode('utf-8'))   # prints the input message

connected = True             # Used as a condition for creating an infinte loop whenever a new thread to a client is established
while connected:             # Infinte loop which keeps waiting to recieve meessages from the client
    actual_message=input()   # [Intialising] the actual_message variable with the user input message
    if actual_message:       # when message has any lenght more the 0 this statement is true
        send(actual_message) # The message gets sent to the server
        if actual_message == Disconnect:#checks for the "End" command if sucessful the client and server disconnect
         send(Disconnect)
                
                

     
                      


       