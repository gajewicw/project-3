import socket                        # a library used to create the server socket
import threading                     # a library use to allow threading which allows multiple client connections

#********************************************************Server Socket Setup***********************************************************
Header_Capacity = 64                 #[Intialising] an intial header space to allow the first message from th client to arrive 
Port_Number = 6000                   # an open port number used as the servers source port
Server_IP = socket.gethostbyname(socket.gethostname()) # This function gets the machines local IP address to be used by the client program to connect.
Server_Address = (Server_IP, Port_Number) # The server address consists of the source port and the machines local IP address
Disconnect = "end"                        #[Intialising] The message from the client that will be used for closing the connection

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # This is were the server socket is created on the local network (AF_INET) using TCP (SOCK_STREAM)
server.bind(Server_Address)                                # The previously established server address is binded to the socket

# ********************************************************Client Connection Thread*****************************************************
def client_connection(connection, host_address):                # A thread which establishes a connection with the client address
    print(f"[new connection] {host_address} connected.")    # A visual prompt acknowleding that a connection has been made [debugging purposes}
     
    Commands=["Info","Withdraw","Deposit","End"] # [Intialising] An array filled with the command names used to filter out error commands from the client
    units=50                                     # [Intialising] Arbitrary value of items on the shelf which in our use case would be genertated from a weight sensor
    Capacity=100                                 # [Intialising] The number of boxes that can fit on the shelf
    Space=Capacity-units                         # [Intialising] The difference of total capacity - filled capacity
# *********************************************************Message Handling*************************************************************
    
    
    connected = True                       # Used as a condition for creating an infinte loop whenever a new thread to a client is established
    while connected:                       # Infinte loop which keeps waiting to recieve meessages from the client
        #######################################YOUTUBE VIDEO################################################
        message_lenght = connection.recv(Header_Capacity).decode('utf-8') # Encodes the string into a byte format
        if message_lenght:                       # checks of the message lenght is more than 0
            message_lenght = int(message_lenght) #Turns the message lenght int an intreger
            message_content = connection.recv(message_lenght).decode('utf-8') # decodes the bytes back into a string
           
          

            print(f"[{host_address}] {message_content}")
         #*****************************************************This is where the commands are established****************************************************
            if message_content=="Info": #checks for the "Info" command if succesful prints the updated information to the client
                connection.send(f"\n{Capacity} storage size\n{units} boxes of gloves available\n{Space}% storage space available\n".encode ('utf-8'))
            if message_content=="Withdraw":#checks for the "Withdraw" command if sucessful updates the value of boxes by -1
                units=units-1
                Space=Capacity-units
                connection.send(f"\n{Capacity} storage size\n{units} boxes of gloves available\n{Space}% storage space available\n".encode ('utf-8')) 
            if message_content=="Deposit":#checks for the "Deposit" command if sucessful updates the value of boxes by +1
                units=units+1
                Space=Capacity-units
                connection.send(f"\n{Capacity} storage size\n{units} boxes of gloves available\n{Space}% storage space available\n".encode ('utf-8'))     
                
            if message_content not in Commands:#checks for the anything that is not a command and sends out an error message to the client 
                connection.send(f"\n [ERROR COMMAND NOT RECOGNISED]".encode ('utf-8'))
                
                          
    if message_content == Disconnect:#checks for the "End" command if sucessful the client and server disconnect
                connected = False
                connection.send(f"closing connection with {host_address}".encode ('utf-8'))
                connection.close()

#*******************************************************************Main Program*****************************************************
def main():                  #This thread is resposnisible for making sure that all the clients can connect simultanously to the server
    server.listen()          #This function allows clients to establish a connection by constanly listening for incoming clients
    print(f" Gloves Shelf is listening on {Server_IP}")# A visual prompt which should print out at the very start of the program [debugging purposes]
    while True:                                        # An infinte loop which allows a theoretically infinte amount of clients to connect
        connection, host_address = server.accept()      # accepts the connection request from the client
        thread = threading.Thread(target=client_connection, args=(connection, host_address)) # sets up a new thread for every client which connects
        thread.start()                                   # Intialises the thread
        count = threading.activeCount() - 1              # Counts the number of threads -1 beacuse one of the thread is used by the server
        print(f"[Active Nodes] {count}")                 # Displays the number of nodes actively connected to the server
        
print(" Glove shelf server intiated...")                 # Prompt which prints at the very start of the program [debugging purposes]
main()